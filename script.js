/* 
1. Новий елемент на сторінці створюється за допомогою методу document.createElement. 
2. Перший параметр вказує на місце вставки елементу відносно елементу, до якого застосовується
insertAdjacentHTML. Всього існує 4 ключових слова, які записуються в перший параметр - 
beforebegin, afterbegin, beforeend, afterend. Перша та остання ключові фрази вставляють
елемент до чи після, а 2 і 3 - вставляють всередину елемента.
3. Для видалення елементів зі сторінки використовується метод remove. Також перенесення елементу
на нове місце автоматично видаляє його з попереднього.
 */
const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
function arrayFunction(array, parent = document.body) {
    parent.insertAdjacentHTML(`beforeend`, `<ul></ul>`);
    const list = document.querySelector(`ul`)
    array.forEach(elem => {
        list.insertAdjacentHTML(`beforeend`, `<li>${elem}</li>`)
    });

}

const div = document.querySelector(`div`);
arrayFunction(array, div);

// console.dir(document.body);